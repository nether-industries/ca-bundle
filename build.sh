#!/bin/sh


prompt_confirm()
{
echo -e "\e[33m: [y,N]\e[0m"
read -sn 1
if [ "$REPLY" == "Y" -o "$REPLY" == "y" ]; then
	return 0
else
	return 1
fi
}
echo -e " \e[32m-\e[0m Building \e[36mca-bundle-nether.pem\e[0m ..."
rm ca-bundle-nether.pem.build

# Looks for all certs ending in '.pem' in ./certs/ and compiles them in ca-bundle-nether.pem.build
find certs/ -iname "*.pem" | xargs -n 1 cat  > ca-bundle-nether.pem.build
echo -n "Copy ca-bundle-nether.pem.build to ca-bundle-nether.pem"
if prompt_confirm; then
	echo -n "Are you sure"
	if prompt_confirm; then
		cp ca-bundle-nether.pem.build ca-bundle-nether.pem
	fi
fi
